package connection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

import query.Query;
import query.QueryHandler;

public class Connection extends Thread{
	private final Socket socket;
	
	public Connection(Socket socket){
		this.socket = socket;
	}
	
	@Override
	public void run(){
		try{
	        PrintWriter pw = new PrintWriter(new OutputStreamWriter(
					socket.getOutputStream(), StandardCharsets.UTF_8), true);
	        BufferedReader br = new BufferedReader(
	        						new InputStreamReader(
	        							socket.getInputStream(), "UTF-8"));
	        String input;
			QueryHandler qh = QueryHandler.getInstance();
			while((input = br.readLine()) != null){				
				if(!input.equals("!q")){
					long id;
					synchronized(qh){
						id = qh.push(new Query(input));
						qh.notifyAll();

						while(!qh.containsRespons(id)){
							try{
								qh.wait();
							}catch(InterruptedException ie){}
						}
						String respons = qh.getRespons(id);
						pw.println(respons);
						pw.flush();
						System.out.println(input + " got respons: " + respons);
						qh.notifyAll();
					}
				}
				else
					break;
			}
			pw.close();
			br.close();
			socket.close();
		}catch(IOException ioe){}
	}
}
