package connection;

import java.io.IOException;
import java.net.ServerSocket;

public class ConnectionHandler extends Thread{
	private ServerSocket serverSocket;
	private final int port = 34567;
	private boolean listen = true;
	
	public ConnectionHandler(){
		try{
			serverSocket = new ServerSocket(port);
			this.start();
		}catch(IOException ioe){
			System.out.println("Couldn't open a socket on port " + port);
			ioe.printStackTrace();
			System.exit(-1);
		}
	}
	
	@Override
	public void run(){
		while(listen){
			try{
				new Connection(serverSocket.accept()).start();
			}catch(IOException ioe){
				System.out.println("Could not establish connection");
				ioe.printStackTrace();
			}
		}
	}
}
