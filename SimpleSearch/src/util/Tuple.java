package util;

public class Tuple<A extends Comparable<A>,B> implements Comparable<Tuple<A,B>>{
	public A x;
	public B y;
	
	public Tuple(A x, B y){
		this.x = x;
		this.y = y;
	}

	public int compareTo(Tuple<A,B> o) {
		return x.compareTo(o.x);
	}
}
