package run;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class SimpleSearch {
	private static long start, stop;
	private static boolean running = true;


	public static void startTimer(String out) {
		System.out.print(out);
		start = System.nanoTime();
	}

	public static void stopTimer() {
		stop = System.nanoTime();
		System.out.println((float) (stop - start) / 1000000 + " ms");
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		try{
			Socket socket = new Socket("rydback.eu", 34567);
			BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream(), "UTF-8"));
			PrintWriter pw = new PrintWriter(new OutputStreamWriter(
					socket.getOutputStream(), StandardCharsets.UTF_8), true);

			
			while (running) {
				System.out.println("\nReady for input:");
				String query = sc.nextLine();
				if (!query.equals("stop")) {
					startTimer("Searching...");
					pw.println(query);
					
					String respons = br.readLine();

					stopTimer();
					System.out.println(query + " is found in: \n" +respons);
					pw.flush();
				} else {
					running = false;
					pw.println("!q");
					System.out.println("Farewell...");
				}
			}
			br.close();
			pw.close();
			socket.close();
		}catch(IOException ioe){
			System.out.println("Couldn't connect to server");
		}
		sc.close();
	}
}
