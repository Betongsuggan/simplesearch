package query;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import util.Tuple;

public class QueryHandler {
	private final static QueryHandler instance = new QueryHandler();
	private final LinkedList<Tuple<Long,Query>> queryQueue = new LinkedList<Tuple<Long,Query>>();
	private final Map<Long,String> responses = new HashMap<Long,String>();
	private long queryID = 0;
	
	private QueryHandler(){
	}
	
	public static synchronized QueryHandler getInstance(){
		return QueryHandler.instance;
	}
	
	public long push(Query q){
		queryQueue.push(new Tuple<Long,Query>(++queryID,q));
		return queryID;
	}
	
	public Tuple<Long,Query> poll(){
		return queryQueue.poll();
	}
	
	public Tuple<Long,Query> peek(){
		//System.out.println(queryQueue.size());
		return queryQueue.peek();
	}
	
	public boolean isEmpty(){
		return queryQueue.isEmpty();
	}
	
	public void putRespons(long id, String respons){
		responses.put(id, respons);
	}
	
	public String getRespons(long id){
		String respons = responses.get(id);
		responses.remove(id);
		return respons;
	}
	
	public boolean containsRespons(long id){
		return responses.containsKey(id);
	}
}
