package query;

import java.util.HashSet;
import java.util.Set;

public class Query {
	Set<String> tokens = new HashSet<String>();
	
	public Query(String query){
		for(String token:query.split("\\s+"))
			tokens.add(token.toLowerCase());
	}
	
	public Set<String> getTokens(){
		return tokens;
	}
	@Override
	public String toString(){
		String returnString = "";
		for(String token:tokens)
			returnString += token + " ";
		return returnString;
	}
}
