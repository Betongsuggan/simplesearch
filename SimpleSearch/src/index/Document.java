package index;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Document implements Serializable{

	private static final long serialVersionUID = 2L;
	private final String name;
	private HashMap<String,Set<Integer>> data;
	
	public Document(String name){
		this.name = name;
		data = new HashMap<String,Set<Integer>>();
	}
	
	public Document(HashMap<String,Set<Integer>> data, String name){
		this.name = name;
		this.data = data;
	}
	
	public HashMap<String,Set<Integer>> getData(){
		return data;
	}
	
	public void setData(List<String> lines){
		int index = 0;
		for(String line:lines){
			String[] tokens = line.split("\\s+");
			for(String token:tokens){
				Set<Integer> indexes = data.get(token.toLowerCase());
				
				if(indexes == null)
					indexes = new HashSet<Integer>();

				indexes.add(index++);
				data.put(token.toLowerCase(),indexes);
			}
		}
	}
	
	public double getTokenFrequency(String token){
		if(data.containsKey(token)){
			return ((double)data.get(token).size())/((double)data.size());
		}
		else
			return 0.0;
	}
	
	public String getName(){
		return name;
	}
	
	public int getSize(){
		return data.size();
	}
}
