package index;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import query.Query;
import query.QueryHandler;
import run.SimpleSearch;
import util.Tuple;
import connection.ConnectionHandler;


public class Indexer extends Thread implements Serializable{

	private static final long serialVersionUID = 1L;
	private Set<Document> corpus;
	private final String indexFile = "index";
	private final String corpusPath = "corpus/";
	private HashMap<String,Set<Document>> wordToDocument;
	private boolean running = true;
	private QueryHandler qh;
	
	public Indexer(){
		qh = QueryHandler.getInstance();
		Indexer indexer = null;// readIndex();

		if(indexer == null){
			corpus = new HashSet<Document>();
			wordToDocument = new HashMap<String,Set<Document>>();
			SimpleSearch.startTimer("Indexing...");
			indexDocuments();
			SimpleSearch.stopTimer();
		}
		else{
			this.corpus 	    = indexer.corpus;
			this.wordToDocument = indexer.wordToDocument;
		}
	}
	
	/*
	 * Adds a document to the index and updates 
	 */
	public void addToIndex(Document doc){
		for(String token:doc.getData().keySet()){
			Set<Document> occurences = wordToDocument.get(token.toLowerCase());

			if(occurences == null)
				occurences = new HashSet<Document>();
			
			occurences.add(doc);
			corpus.add(doc);
			wordToDocument.put(token.toLowerCase(), occurences);
		}		
	}
	
	/*
	 * Given a search query, it returns a list of documents in which the query has some occurences.
	 * The list is sorted after its cosine simularity.
	 */
	
	
	//-----------------------TODO COSINE SIM------------------------------
	public List<Tuple<Double,Document>> search(Query query){
		List<Tuple<Double,Document>> results = new LinkedList<Tuple<Double,Document>>();
		Map<Document,Double> scores = new HashMap<Document,Double>();
		
		for(String token:query.getTokens()){
			if(wordToDocument.containsKey(token)){
				double occurences = wordToDocument.get(token).size();
				for(Document doc:wordToDocument.get(token)){
					double idf = Math.log10(((double)corpus.size())/occurences);
					double tf_idf = doc.getTokenFrequency(token)*idf;
					
					double oldTf_idf = 0.0;
					if(scores.containsKey(doc)){
						oldTf_idf = scores.get(doc);
					}
					
					scores.put(doc,oldTf_idf+tf_idf);
				}
			}
		}
		
		for(Document doc:scores.keySet())
			results.add(new Tuple<Double,Document>(scores.get(doc),doc));
		
		results.sort(new Comparator<Tuple<Double,Document>>(){

			public int compare(Tuple<Double, Document> arg0,Tuple<Double, Document> arg1) {
				return -1*arg0.compareTo(arg1);
			}		
	
		});
		return results;
	}

	/*
	 * Given a file path it adds all .txt documents to the indexer.
	 */
	private void indexDocuments() {
		Set<File> documents = getAllSubFiles(new File(corpusPath));

		for (File file : documents) {
			try {
				Document doc = new Document(file.getName());
				doc.setData(Files.readAllLines(file.toPath(), StandardCharsets.UTF_8));
				addToIndex(doc);

			} catch (IOException e) {
				System.out.println("Cannot read from file: " + file.toPath());
			}
		}
	}
	
	/* 
	 * Given a file path, it recursively finds and returns all .txt files.
	 */
	private Set<File> getAllSubFiles(File file) {
		Set<File> subCorpus = new HashSet<File>();

		if (file.isFile()) {
			Pattern txt = Pattern.compile("\\w+.txt");
			Matcher fileNameMatcher = txt.matcher(file.getName());
			//String fileType = file.getName().split("\\.")[1];

			if (fileNameMatcher.find()) {
				subCorpus.add(file);
			}
		} else if (file.isDirectory()) {
			for (String child : file.list()) {
				if (!child.equals("org") && !child.startsWith(".")) {
					subCorpus.addAll(getAllSubFiles(new File(file.getPath()
							+ "/" + child)));
				}
			}
		}

		return subCorpus;
	}
	
	/* Repeatedly checks QueryHandler for new queries. If some query exists, it searches the query and posts a
	 * respons in QueryHandler. Accesses QueryHandler in a synchronized fashion and waits for other threads notify
	 * until it checks for new queries. No deadlock testing has been done.
	 * 
	 * (non-Javadoc)
	 * @see java.lang.Thread#run()
	 */
	@Override
	public void run() {
		while (running) {
			synchronized(qh){
				if (!qh.isEmpty()) {
					Tuple<Long,Query> query = qh.poll();
					
					List<Tuple<Double, Document>> result = new ArrayList<Tuple<Double,Document>>();
					
					result = search(query.y);
	
					StringBuilder sb = new StringBuilder();
					
					if (result.size() != 0) {
						DecimalFormat df = new DecimalFormat("0.000");
						Iterator<Tuple<Double, Document>> it = result.iterator();
						
						while (it.hasNext()) {
							Tuple<Double, Document> pair = it.next();
							sb.append(pair.y.getName() + ":" + df.format(pair.x) + " ");
						}
					} 
					else
						sb.append("No results for " + query.y + "....");
					
					qh.putRespons(query.x, sb.toString());
					qh.notifyAll();
				}
				try{
					qh.wait();
				}catch(InterruptedException ie){}
			}
		}
		//saveIndex();	
	}
	
	/*
	 * Method for reading an index file from file.
	 * 
	 * Current implementation unbelievably slow.
	 */	
	private Indexer readIndex(){
		Indexer instance = null;
		try{
			RandomAccessFile raf = new RandomAccessFile(indexFile, "rw");
			FileInputStream fis = new FileInputStream(raf.getFD());
			ObjectInputStream ois = new ObjectInputStream(fis);
			instance = (Indexer)ois.readObject();
			raf.close();
			ois.close();
			fis.close();
			ois.close();
		}catch(IOException ioe){
			System.out.println("Could not find " + indexFile + ", starting new index");
		}catch(ClassNotFoundException cnfe){
		}
		
		return instance;
	}
	
	/*
	 * Method for saving down the index class to file.
	 * 
	 * Current implementation unbelievably slow.
	 */
	public void saveIndex(){
		try{
			RandomAccessFile raf = new RandomAccessFile(indexFile, "rw");
			FileOutputStream fos = new FileOutputStream(raf.getFD());
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(this);
			raf.close();
			fos.close();
			oos.close();
			System.out.println("Saved index to " + indexFile);
		}catch(IOException ioe){
			System.out.println("Could not write index to file...");
			ioe.printStackTrace();
			System.exit(0);
		}
	}
	
	/*
	 * Main method for the server
	 */
	public static void main(String[] args){
		new ConnectionHandler();
		new Indexer().start();
		System.out.println("Server started");
	}
}
